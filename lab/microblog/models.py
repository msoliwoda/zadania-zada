from django.db import models

# Create your models here.
class Wpis(models.Model):
    tytul = models.CharField(max_length=200)
    tresc = models.TextField(max_length=2000)
    podpis = models.CharField(max_length=50)


    def __unicode__(self):
        return self.tytul