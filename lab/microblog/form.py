from django import forms
from models import Wpis

class WpisForm(forms.ModelForm):

    class Meta:
        model = Wpis
        fields = ('tytul', 'tresc')