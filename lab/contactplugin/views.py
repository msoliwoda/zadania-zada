# Create your views here.
from django.shortcuts import render_to_response
from django.template import *
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from models import ContactForm

def thanks(request):
    return render_to_response("wyslano.html", context_instance = RequestContext(request))

def contact(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ContactForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            recipients = ['mcprimo@autograf.pl']
            if cc_myself:
                recipients.append(sender)

            from django.core.mail import send_mail
            #return HttpResponseRedirect('/thanks/') #dla testow
            send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect("http://194.29.175.240/~p18/djangocms.wsgi/pl/kontakt/thanks/") # Redirect after POST
    else:
        form = ContactForm() # An unbound form

    return render_to_response("contactplugin.html", {'form': form, }, context_instance = RequestContext(request))