from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from menu import BlogMenu
from django.utils.translation import ugettext_lazy as _

class MysiteApp(CMSApp):
    name = _("Mystie App") # give your app a name, this is required
    urls = ["mysite.urls"] # link your app to url configuration(s)
    menus = [BlogMenu]

apphook_pool.register(MysiteApp) # register your app