from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.core.context_processors import csrf
from django.template import RequestContext


def register_user(request):
    form = UserCreationForm(request.POST)
    args= {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://194.29.175.240/~p18/djangocms.wsgi/pl/blog/accounts/register_success')
        else:
            args['wiad'] = "Wypelnij poprawnie wszystkie pola lub takie konto juz istnieje"
    print args
    return render_to_response('register.html', args,
                            context_instance=RequestContext(request),)

def register_success(request):
    return render_to_response('register_success.html',
                            context_instance=RequestContext(request),)


def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c,
                                context_instance=RequestContext(request),)

def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username = username, password=password)

    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect('http://194.29.175.240/~p18/djangocms.wsgi/pl/blog/accounts/loggedin')
    else:
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c,
                              context_instance=RequestContext(request),)

def loggedin(request):
    return render_to_response('loggedin.html',
                                {'full_name': request.user.username},
                              context_instance=RequestContext(request),)

def invalid_login(request):
    return render_to_response('invalid_login.html')

def logout(request):
    auth.logout(request)
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c,
                              context_instance=RequestContext(request),)