from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

class BlogMenu(CMSAttachMenu):
    name = _("Blog Menu") # give the menu a name, this is required.

    def get_nodes(self, request):
        """
        This method is used to build the menu tree.
        """
        nodes = []
        n1 = NavigationNode(
                "Blog",
                "http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/blog/",
                1
         )
        n2 = NavigationNode(
            "Dodaj Wpis",
            "http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/blog/create/",
            2
        )
        n3 = NavigationNode(
            "Admin",
            'http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/admin/',
            3
        )
        n4 = NavigationNode(
            "Zaloguj",
            "http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/accounts/login/",
            4
        )
        n5 = NavigationNode(
            "Wyloguj",
               "http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/accounts/logout/",
            5
        )
        n6 = NavigationNode(
            "Rejestracja",
            "http://194.29.175.240/~p18/djangocms.wsgi/pl//blog/accounts/register/",
            6
        )
        nodes.append(n1)
        nodes.append(n2)
        nodes.append(n3)
        nodes.append(n4)
        nodes.append(n5)
        nodes.append(n6)
        return nodes
menu_pool.register_menu(BlogMenu) # register the menu.