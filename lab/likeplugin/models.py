from django.db import models
from cms.models.pluginmodel import CMSPlugin

class LikeItPlugin(CMSPlugin):
    url = models.URLField(max_length=600)
