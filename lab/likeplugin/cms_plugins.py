from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from models import LikeItPlugin

class LikeButtonPlugin(CMSPluginBase):
    model = LikeItPlugin
    name = _("Like plugin") # Name of the plugin
    render_template = "likeplugin.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        # context.update({'instance':instance})
        context['instance']=instance
        return context

plugin_pool.register_plugin(LikeButtonPlugin) # register the plugin